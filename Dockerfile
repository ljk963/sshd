FROM alpine:latest

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories \
    && apk add --no-cache tzdata curl bash busybox-extras vim openssh-client openssh-server \
    && rm -rf /var/cache/apk/*
CMD [ "/usr/sbin/sshd" ]
EXPOSE 22